import * as React from "react";
import "./App.css";

// Animate GLB
import * as THREE from "three"
import Stats from "three/examples/jsm/libs/stats.module.js";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { RoomEnvironment } from "three/examples/jsm/environments/RoomEnvironment.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";

// WebXR: TODO
//import { Box,/*OrbitControls*/ } from "@react-three/drei"
//import { useFrame, useLoader } from "@react-three/fiber"

const setup = () => {
  const clock = new THREE.Clock();
  const container = document.getElementById( "VRBox" );
  if (!container) return;

  // Setup renderer
  const renderer = new THREE.WebGLRenderer( {antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.outputEncoding = THREE.sRGBEncoding;
  container.appendChild( renderer.domElement );

  const pmremGenerator = new THREE.PMREMGenerator( renderer );

  // Setup scene & camera
  const scene = new THREE.Scene()
  scene.background = new THREE.Color( 0xbfe3dd );
  scene.environment = pmremGenerator.fromScene( new RoomEnvironment(), 0.04 ).texture;

  const camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.1, 100 );
  camera.position.z = 3;

  //orbit controls
  const controls = new OrbitControls( camera, renderer.domElement );
  controls.target.set( 0, 0.5, 0 );
  controls.update();
  controls.enablePan = true;
  controls.enableDamping = true;

  const dracoLoader = new DRACOLoader();
  dracoLoader.setDecoderPath( "../node_modules/three/examples/js/libs/draco/gltf/" );

  const loader = new GLTFLoader();
  loader.setDRACOLoader( dracoLoader );
  loader.load( "./house.glb", (gltf) => {
    const model = gltf.scene;
    model.position.set( 0, 0, 0 );
    model.scale.set( 0.2, 0.2, 0.2 );
    scene.add( model );

    //animate
    animate();

  }, undefined, (error) => {
    console.log( error );
  });

  const animate = () => {
    requestAnimationFrame( animate );
    controls.update();
    renderer.render( scene, camera );
  }

}

function App() {

  return (
    <div id="VRBox">
      <button onClick={setup}> Start Setup </button>
    </div>
  );
}

export default App;
